#!/bin/bash

##SLURM
#SBATCH -N 1
## at -j 20, we're lucky to use 20% of the cpu (quick jobs, so mostly in py and sh)
#SBATCH -c 5
#SBATCH -p scope-shared.q
#SBATCH -J cm_chunk
#SBATCH -t 15-00:00
#SBATCH --array=1-100%20
#SBATCH -o slurm/model.build.%A_%a.out
#SBATCH -e slurm/model.build.%A_%a.out

# get env data from SLURM or SGE
M=${SLURM_ARRAY_TASK_ID}
let N=M-1
CHUNK=`printf %03d $N`

# save dir for cleanup at end
CWD=$(pwd)

FLAT_DIR=${1:-cluster_chunks}
OUT_DIR=${2:-model_chunks}
TMP_DIR=${3:-${CWD}}
SCRIPT_DIR=${4:-$(pwd)}
SPLIT_SIZE=${5:-0}
SPLIT_INDEX=${6:-0}
CLEANUP=${7:-YES}

OUT_DIR=`readlink -f ${OUT_DIR}`
FLAT_DIR=`readlink -f ${FLAT_DIR}`

FLAT_FILE=$FLAT_DIR/seqs_in_group_${CHUNK}.flat.sort
MODEL_FILE=$OUT_DIR/models_from_${CHUNK}.cm
mkdir -p $OUT_DIR

# set up env for model building
source activate structure

# temp dir for running
WD=$TMP_DIR/tmp_chunk_${CHUNK}_batch_${SPLIT_INDEX}
mkdir -p $WD
cd $WD

echo "running snakemake in $(pwd)"
snakemake -s ${SCRIPT_DIR}/build_model.chunk.snake -p -j 20 -k \
    --config chunk_file="${FLAT_FILE}" \
             model_file="${MODEL_FILE}" \
             split_size=$SPLIT_SIZE \
             split_index=$SPLIT_INDEX

# report the benchmarks
echo BENCHMARKS
find benchmarks -type f | xargs head

# clean up files
if [ "$CLEANUP" == "YES" ]; then
    cd $CWD
    rm -rf $WD
fi
