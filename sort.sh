#!/bin/bash

##SLURM
#SBATCH -N 1
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -p scope-shared.q
#SBATCH -J sort
#SBATCH -t 00:10:00
#SBATCH -o slurm/sort.%A.out
#SBATCH -e slurm/sort.%A.out

sort $1 > $1.sort
