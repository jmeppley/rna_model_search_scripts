#!/usr/bin/env python
"""
split_into_chunks

splits sequence flat file into 100 smaller files with:
 * no seqs from clusters w/ <5 seqs
 * cluster id in first column

Usage:
    split_into_chunks.py <flat_file> <cluster_file> <out_dir>

Options:
    -N=<chunks>     Split into N chunks [default: 100]
    -c=<cutoff>     Ignore clusters with < cutoff sequences [default: 5]
    -l=<loglevel>      Set log level to loglevel [default: 20]
"""
import os
import logging
from docopt import docopt

logger = logging.getLogger('split_into_chunks')

def main(arguments):
    """
    simply translates the docopt arguments dict into arguments for the
    split_flat_file() function
    """
    logger.setLevel(int(arguments['<loglevel>']))
    split_flat_file(arguments['<flat_file>'],
                    arguments['<cluster_file>'],
                    arguments['<out_dir>'],
                    chunks=int(arguments.get('<chunks>', 100)),
                    cutoff=int(arguments.get('<cutoff>', 5)),
                   )

def split_flat_file(flat_file, cluster_file, out_dir, chunks=100, cutoff=5):
    """ does the basic work """
    # first parse cluster file and assign groups st reads per group is same
    cluster_counts, cluster_membership = parse_cluster_file(cluster_file)
    cluster_groups = divide_clusters_into_groups(cluster_counts,
                                                 chunks=chunks,
                                                 cutoff=cutoff
                                                )
    cluster_to_group_map = get_cluster_group_map(cluster_groups)

    logger.info("Creating {} chunks with {} to {} clusters".format(
        chunks,
        min([len(g) for g in cluster_groups.values()]),
        max([len(g) for g in cluster_groups.values()]),
    ))

    # set up one output file per chunk
    cluster_group_handles = [open(os.path.join(out_dir, \
                                    'seqs_in_group_{0:03d}.flat'.format(i)),
                                  'wt') \
                             for i in range(chunks)]

    # loop over all seqs and write to one of the files (or skip)
    mark = 100
    mark_step = 2
    with open(flat_file) as SEQS:
        for i, line in enumerate(SEQS):
            seqid, sequence = line.rstrip().split(None, 1)
            cluster = cluster_membership.get(seqid, '0')
            if cluster == '0':
                # ignore singles
                continue
            count = cluster_counts[cluster]
            if count < cutoff:
                # ignore small clusters
                continue
            group = cluster_to_group_map[cluster]
            handle = cluster_group_handles[group]
            handle.write("{cluster}\t{seqid}\t{sequence}\n".format(**vars()))

            if i+1 > mark:
                mark = mark * mark_step
                logger.debug("Processed {}th seq {}" \
                        .format(i, seqid))

    # close handles
    for handle in cluster_group_handles:
        handle.close()

def parse_cluster_file(cluster_file):
    """
    parse the cluster file into two dicts:
        map from cluster id to number of seqs per cluster
        map from seqid to cluster id

    returned as 2-elelment tuple
    """
    cluster_membership = {}
    cluster_counts = {}
    with open(cluster_file) as CLUST:
        for line in CLUST:
            clid, count, members = line.strip().split('\t', 2)

            count = int(count)
            members = members.split()
            if len(members) != count:
                print("ERROR: count mismatch!")
                break

            cluster_counts[clid] = count
            for seqid in members:
                cluster_membership[seqid] = clid

    return (cluster_counts, cluster_membership)

def divide_clusters_into_groups(cluster_counts, chunks=100, cutoff=5):
    """
    bin the cluster ids into groups so that a similar number of seqs is in each
    group. Set the number of groups with chunks. Ignore clusters with fewer
    than
    cutoff sequences
    """
    clusters_by_size = sorted(cluster_counts.keys(), key=lambda c: cluster_counts[c], reverse=True)
    cluster_groups = {i:[] for i in range(chunks)}
    cluster_group_seq_counts = {i:0 for i in range(chunks)}
    for c in clusters_by_size:
        count = cluster_counts[c]
        if count < cutoff:
            break
        biggest_yet = 0
        smallest_yet = 2000000000000
        smallest_id = None
        for g in range(chunks):
            group_size = cluster_group_seq_counts[g]
            if group_size < smallest_yet:
                smallest_yet = group_size
                smallest_id = g
            if group_size > biggest_yet:
                biggest_yet = group_size
                continue
            if group_size + count < biggest_yet:
                cluster_groups[g].append(c)
                cluster_group_seq_counts[g] += count
                break
        else:
            # just put in the smallest
            cluster_groups[smallest_id].append(c)
            cluster_group_seq_counts[smallest_id] += count
    return cluster_groups

def get_cluster_group_map(cluster_groups):
    """ return flipped dict that maps cluster ids to group index """
    cluster_to_group_map = {}
    for g in cluster_groups:
        for c in cluster_groups[g]:
            cluster_to_group_map[c] = g
    return cluster_to_group_map

# hook so that script behavior is contained in a main() function
if __name__ == '__main__':
    docopt_args = docopt(__doc__, version='untested')
    main(docopt_args)
