#!/bin/bash
#SBATCH -N 1
## it doesn't need 10 threads, but it does need the RAM that comes with them
#SBATCH -c 10
#SBATCH -p scope-shared.q
#SBATCH -J cluster_split
#SBATCH -o slurm/chunks.%A.out
#SBATCH -e slurm/chunks.%A.out
#SBATCH -t 01:30:00

FLAT_FILE=${1}
CLUSTER_FILE=${2}
OUT_DIR=${3}
CM_DIR=${4:-$(pwd)}

source activate structure
mkdir -p $OUT_DIR
echo ${CM_DIR}/split_into_chunks.py $FLAT_FILE $CLUSTER_FILE $OUT_DIR
time ${CM_DIR}/split_into_chunks.py $FLAT_FILE $CLUSTER_FILE $OUT_DIR
