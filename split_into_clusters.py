#!/usr/bin/env python
"""
split_into_clusters

Creates a tree of fasta files for each cluster in flat file.

Optionally can paginate the flat file and return just the nth group of N clusters

Usage:
    split_into_clusters.py [-n=<n> -N=<N>] <flat_file> <out_dir>

Options:
    -n=<n>   Process only the n'th cluster (0 is the first)
    -N=<N>   Split file in groups of N clusters (0 => all)
"""
import os
from docopt import docopt

def main(arguments):
    """
    simply translates the docopt arguments dict into arguments for the
    split_flat_file() function
    """
    split_flat_file(arguments['<flat_file>'],
                    arguments['<out_dir>'],
                    split_size=int(arguments.get('<N>', 0)),
                    split_index=int(arguments.get('<n>', 0)),
                   )


def get_cluster_ids_old(flat_file, split_size=0, split_index=0):
    """ get the list of cluster ids from a fragment of the flat file

        This version reads all cluster ids, then slices the list.
    """
    cluster_ids = []
    last_cluster_id = None
    with open(flat_file) as F:
        for line in F:
            cluster_id = line.split('\t', 1)[0]
            if cluster_id != last_cluster_id:
                cluster_ids.append(cluster_id)
                last_cluster_id = cluster_id


    start_index = split_index * split_size
    end_index = start_index + split_size if split_size > 0 else None
    return cluster_ids[start_index:end_index]


def get_cluster_ids(flat_file, split_size=0, split_index=0):
    """ get the list of cluster ids from a fragment of the flat file """
    cluster_ids = []
    last_cluster_id = None
    cluster_count = -1
    seq_count = 0
    line_count = 0
    start_cluster = split_index * split_size
    end_cluster = (split_index + 1) * split_size
    currently_capturing = False
    with open(flat_file) as flat_handle:
        for line in flat_handle:
            line_count += 1
            cluster_id, seqid, sequence = line.rstrip().split()
            if cluster_id != last_cluster_id:
                last_cluster_id = cluster_id

                # handle pagination
                cluster_count += 1
                if end_cluster > 0 and cluster_count >= end_cluster:
                    break
                capture_cluster_id = (split_size == 0) \
                    or (cluster_count >= start_cluster)
                if capture_cluster_id:
                    cluster_ids.append(cluster_id)
    return cluster_ids


def split_flat_file(flat_file, destination, split_size=0, split_index=0):
    """
    split the sequences in flat_file into
    fasta files in the destination directory

    optionally can process just a subset by spcifying split_+size and split_index.
    """
    last_cluster_id = None
    out_handle = None
    print("splitting " + flat_file)
    cluster_count = -1
    seq_count = 0
    line_count = 0
    start_cluster = split_index * split_size
    end_cluster = (split_index + 1) * split_size
    currently_capturing = False
    with open(flat_file) as flat_handle:
        for line in flat_handle:
            line_count += 1
            cluster_id, seqid, sequence = line.rstrip().split()
            if cluster_id != last_cluster_id:
                last_cluster_id = cluster_id
                if out_handle is not None:
                    out_handle.close()

                # handle pagination
                cluster_count += 1
                if end_cluster > 0 and cluster_count >= end_cluster:
                    break
                currently_capturing = (split_size == 0) \
                    or (cluster_count >= start_cluster)

                # set up output handle if needed
                if currently_capturing:
                    rel_folder = get_cluster_folder(cluster_id)
                    cluster_folder = os.path.join(destination, rel_folder)
                    os.makedirs(cluster_folder, exist_ok=True)
                    cluster_fasta = os.path.join(cluster_folder, 'cluster.fasta')
                    out_handle = open(cluster_fasta, 'at')
            if currently_capturing:
                seq_count += 1
                out_handle.write('>{}\n{}\n'.format(seqid, sequence))
    if out_handle is not None:
        out_handle.close()
    print("Processed {} of {} seqs in {} clusters".format(seq_count,
                                                          line_count,
                                                          cluster_count))

def get_fasta_from_flat_file(flat_file, fasta_file, target_cluster_id):
    """
    pull out the sequences for a given cluster into a fasta file
    """
    seq_count = 0
    line_count = 0
    with open(fasta_file, 'wt') as out_handle:
        with open(flat_file) as flat_handle:
            for line in flat_handle:
                line_count += 1
                cluster_id, seqid, sequence = line.rstrip().split()
                if cluster_id == target_cluster_id:
                    seq_count += 1
                    out_handle.write('>{}\n{}\n'.format(seqid, sequence))

    print("Found  {} seqs in {} lines".format(seq_count,
                                              line_count,))


def get_cluster_folder(clid):
    """
    Returns a multi level relative path for storing this cluster alongside
    thousands of others without putting too many in a single folder.

    EG: cluster id 12345 becomes 1/23/45/cluster_12345
    """
    segments = [clid[0]]
    remainder = clid[1:]
    while len(remainder) > 1:
        segments.append(remainder[:2])
        remainder = remainder[2:]
    segments.append("cluster_" + clid)
    cluster_folder = os.path.join(*segments)
    return cluster_folder

# hook so that script behavior is contained in a main() function
if __name__ == '__main__':
    docopt_args = docopt(__doc__, version='untested')
    main(docopt_args)
