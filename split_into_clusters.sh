#!/bin/bash
#SBATCH -N 1
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -C haswell
#SBATCH -p shared
#SBATCH -J cluster_split
#SBATCH -o slurm/split.%A.%a.out
#SBATCH -e slurm/split.%A.%a.out
#SBATCH --mail-user=jmeppley@lbl.gov
#SBATCH --mail-type=ALL
#SBATCH -t 00:30:00
#SBATCH --array=1-100
#DW persistentdw name=build_cms 

CM_DIR=/projectb/scratch/jmeppley/RNA/CMs

FLAT_DIR=${1:-${CM_DIR}/sandbox/split-flat-seqs/splits_1}
OUT_ROOT=${DW_PERSISTENT_STRIPED_build_cms}

N=$SLURM_ARRAY_TASK_ID
let N=N-1
N=`printf %03d $N`
OUT_DIR=$OUT_ROOT/fasta_$N
mkdir -p $OUT_DIR
FLAT_FILE=$FLAT_DIR/seqs_in_group_${N}.flat.sort

source activate structure
echo ${CM_DIR}/split_into_clusters.py $FLAT_FILE $OUT_DIR
time ${CM_DIR}/split_into_clusters.py $FLAT_FILE $OUT_DIR

cd $OUT_DIR && find . -type f -name cluster.fasta > ./fasta.files.list

grep -c -H . $OUT_DIR/fasta.files.list
